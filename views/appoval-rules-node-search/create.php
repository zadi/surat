<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppovalRulesNodeSearch */

$this->title = 'Create Appoval Rules Node Search';
$this->params['breadcrumbs'][] = ['label' => 'Appoval Rules Node Searches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appoval-rules-node-search-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
